
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


// example mapper will modify the lowPass parem (lpHz) as a function of azimuth, such that the lowPass filtering effect is increased as the aziDeg incidence angle is panned away from the center.

~name = \muffleMapper;
~function = {
	// arguments, starting with required spatializer parameters:
	| aziDeg = 0, eleDeg = 0, gainDB = -90, delayMs = 1, lpHz = 15000, hpHz = 0.5, spread = 1|

	// computing value to be sent to the spatializer:
	var newLpHz;
	var thetaScaler = aziDeg.clip(-90, 90) * 0.011111111111111;  // 1/90
	newLpHz = (1 - thetaScaler.abs) * (lpHz - 750) + 750;


	// forwarding required spatializer parameters only,
	// here lpHz has been replaced by the computed value
	// Note that mapper must return an array of array(s), allowing specific mapping for
	// specific spatializers, see the nearFarField.scd file for more elaborate example
	[[aziDeg, eleDeg, gainDB, delayMs, newLpHz, hpHz, spread]];
};


~aziDeg=0;
~aziDeg=45;



(
~aziDeg= -100;

~lpHz = 13000;
~thetaScaler = ~aziDeg.clip(-90, 90)*0.011111111111111;

~newLpHz = (1 - ~thetaScaler.abs) * (~lpHz-750) + 750;

)