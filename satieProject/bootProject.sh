# Generic Satie Project Loader
# depends on the following directory structure
#
# within this containing directory, there needs to be:
#     ONLY one  ".scd" file, that corresponds to the project file
#     a directory called autoload, containing any number of ".scd" files that will be loaded after the project file is loaded
#
# args:   optiontal flags:
#       -f  listeningFormat {sato,labo}   (default == stereo)
#       -b  effects bus count   (default == 4)
# no args defaults to stereo format
#
#  ENVIRONMENT VARIABLES:
#
#   SUPERCOLLIDER defaults to /Applications/SuperCollider.app for OSX



DIR_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

OS=`uname -s`

SATIE_DIR=${DIR_PATH}/..

#default project file to load
LAUNCH_FILE=$DIR_PATH/*.scd


#  if  $SUPERCOLLIDER poins to a  sclang executable, it will be called, otherwise will launch using default path to sclang


if [ "$OS" = "Darwin" ]; then
    if [ -z  $SUPERCOLLIDER ]  ; then
    $SUPERCOLLIDER=/Applications/SuperCollider.app
    echo bootSatie.sh:  guessing that supercollider is at:  $SUPERCOLLIDER

    fi

    SUPERCOLLIDER_APP=$SUPERCOLLIDER/Contents/MacOS/sclang
fi



if [ "$OS" = "Linux" ]; then
        if [ -z  $SUPERCOLLIDER ]  ; then
    $SUPERCOLLIDER=/use/local/bin/sclang
        echo bootProject.sh:  guessing that supercollider is at:  $SUPERCOLLIDER_APP

    fi
    SUPERCOLLIDER_APP=$SUPERCOLLIDER
fi

echo using Supercollider executable located at:  $SUPERCOLLIDER_APP, with loadfile:  $LAUNCH_FILE




if [ -z  $MY_IP ] ; then
$MY_IP=localhost
fi




# note: if this is set to -gt 0 the /etc/hosts part is not recognized ( may be a bug )

if [ $# -eq  1 ] && [ $1 == "-h" ] ;    then
echo $0 help: "this script will launch supercollider and load the project's .scd file contained in this directory"
echo $0 help: "optional flags: -f or -listenerFormat  {sato, labo, stereo, octo, mono, 5one} (default == stereo)"
exit
fi


if [ $# -eq  0 ] ;    then
echo $0 : No arguments, using defaults:  project file = $LAUNCH_FILE,  listening format: $LISTENING_FORMAT
fi





while [[ $# -gt 1 ]]
do
key="$1"

case $key in
-f|--LISTENERFORMAT)
LISTENING_FORMAT="$2"
shift # past argument
;;
--default)
DEFAULT=YES
;;
*)
# unknown option
;;
esac
shift # past argument or value
done


#echo LISTENER FORMAT     = "${LISTENING_FORMAT}"

echo
echo $0 : "booting SATIE on " $MY_IP":18032"
echo $0: calling $SUPERCOLLIDER_APP with args: $LAUNCH_FILE $LISTENING_FORMAT
echo


$SUPERCOLLIDER_APP $LAUNCH_FILE $LISTENING_FORMAT


exit

