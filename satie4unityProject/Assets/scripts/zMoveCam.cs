﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class zMoveCam : MonoBehaviour
{
    public float Speed = .5f;

    void Update()
    {
        float xAxisValue = Input.GetAxis("Horizontal") * Speed;
        float zAxisValue = Input.GetAxis("Vertical") * Speed;
        float yValue = 0.0f;

        if (Input.GetKey(KeyCode.Q))
        {
            yValue = -Speed/4;
        }
        if (Input.GetKey(KeyCode.E))
        {
            yValue = Speed/4;
        }

        transform.position = new Vector3(transform.position.x + xAxisValue, transform.position.y + yValue, transform.position.z + zAxisValue);
    }
}