﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class zSceneManager : MonoBehaviour
{

    private bool loadScene = false;
    public int sceneCount=0;
    private bool showGui;
    private int currentSceneIndex = -1;

    public GameObject UIpanel;

    public int emptySceneIndex;


    private Scene currentScene;

    public Text loadingText;

    private string loadingTextStr;
    private UnityEngine.Color loadingTextColor;

    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
        loadingTextColor = loadingText.color;
        loadingTextStr = loadingText.text;
        showGUI(showGui = true);
    }

    // Updates once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Space) || Input.GetKeyUp(KeyCode.Escape))
        {
            showGui = !showGui;
            showGUI(showGui);
        }

        if (loadScene == true)
        {


            // ...change the instruction text to read "Loading..."
            loadingText.text = "Loading scene " + currentSceneIndex + "/" + sceneCount;

            // ...then pulse the transparency of the loading text to let the player know that the computer is still working.
            loadingText.color = new Color(loadingText.color.r, loadingText.color.g, loadingText.color.b, Mathf.PingPong(Time.time, 1));

        }

    }

    public void showGUI(bool state)
    {
        UIpanel.SetActive(state);
        if (currentSceneIndex == -1) return;

        if (state) SceneManager.LoadScene(emptySceneIndex);

        //if (currentScene.isLoaded)
        //{
        //    SceneManager.UnloadScene(currentScene.name);
        //}

    }


    public void quitGame()
    {
        Application.Quit();
    }

    public void loadSceneN(int index)
    {
        Debug.Log("loadSceneN LOADING SCENE: " + index);  // return if the scene loader is busy
        if (loadScene == true) return;



        // ...set the loadScene boolean to true to prevent loading a new scene more than once...
        loadScene = true;


        if (index < 0) index = 0;
        else if (index > sceneCount - 1) index = sceneCount - 1;

        currentSceneIndex = index;

        StartCoroutine(LoadNewScene(currentSceneIndex));
    }



    // The coroutine runs on its own at the same time as Update() and takes an integer indicating which scene to load.
    IEnumerator LoadNewScene(int index)
    {
        Debug.Log("LoadNewScene LOADING SCENE: " + index);
        // This line waits for 3 seconds before executing the next line in the coroutine.
        // This line is only necessary for this demo. The scenes are so simple that they load too fast to read the "Loading..." text.
        //yield return new WaitForSeconds(3);

        // Start an asynchronous operation to load the scene that was passed to the LoadNewScene coroutine.
        AsyncOperation async = Application.LoadLevelAsync(index);

        // While the asynchronous operation to load the new scene is not yet complete, continue waiting until it's done.
        while (!async.isDone)
        {
            yield return null;
        }
        loadingText.color = loadingTextColor;
        loadingText.text = loadingTextStr;
        loadScene = false;
        showGUI(showGui = false);

        currentScene = SceneManager.GetActiveScene();

    }

}